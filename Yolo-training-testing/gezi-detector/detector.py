import sys
import numpy as np
import nms


import torch
import torchvision
import cv2


np.set_printoptions(threshold=sys.maxsize)

model = cv2.dnn.readNetFromDarknet('yolov3-spp.cfg','gezi-detector-v2.1.weights')

im = cv2.imread("images/99022.jpg")
blob = cv2.dnn.blobFromImage(im, 1, (416, 416), swapRB=True, crop=False)
model.setInput(blob)
prediction = model.forward()

print(prediction)

w = prediction[:,2]
h = prediction[:,3]

x1 = (prediction[:,0] - w/2).reshape((len(prediction[:,0]), 1))
y1 = (prediction[:,1] - h/2).reshape((len(prediction[:,1]), 1))
x2 = (prediction[:,0] + w/2).reshape((len(prediction[:,0]), 1))
y2 = (prediction[:,1] + h/2).reshape((len(prediction[:,1]), 1))

boxes = np.concatenate((x1, y1, x2, y2), axis=-1)
scores = prediction[:,4]


def nms(dets, scores, thresh):
    '''
    dets is a numpy array : num_dets, 4
    scores ia  nump array : num_dets,
    '''
    x1 = dets[:, 0]
    y1 = dets[:, 1]
    x2 = dets[:, 2]
    y2 = dets[:, 3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1] # get boxes with more ious first

    keep = []
    while order.size > 0:
        i = order[0] # pick maxmum iou box
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1) # maximum width
        h = np.maximum(0.0, yy2 - yy1 + 1) # maxiumum height
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)

        inds = np.where(ovr <= thresh)[0]
        order = order[inds + 1]

    return keep

boxes = torch.Tensor(list(boxes))
scores = torch.Tensor(list(scores))

indices = torchvision.ops.nms(boxes, scores, 0.3) #nms(boxes, scores, 0.3)

x1 = x1[indices]
y1 = y1[indices]
x2 = x2[indices]
y2 = y2[indices]
scores = scores[indices]

bounding_boxes = (np.concatenate((x1, y1, x2, y2), axis=-1) * 416).astype(int)

bounding_boxes = bounding_boxes[scores>0.9]
scores = scores[scores>0.9]

for i in range(len(bounding_boxes)):
	rx1 = bounding_boxes[i][0]
	ry1 = bounding_boxes[i][1]
	rx2 = bounding_boxes[i][2]
	ry2 = bounding_boxes[i][3]
	im[ry1:ry2, rx1:rx2, 0] = 255
	im[ry1:ry2, rx1:rx2, 1] = 205


cv2.imshow("image", im)
cv2.waitKey(0)
cv2.destroyAllWindows()
