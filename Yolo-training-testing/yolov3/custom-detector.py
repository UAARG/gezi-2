from models import *
from utils.datasets import *
from utils.utils import *

cfg_path = 'cfg/yolov3-spp.cfg'
img_size = (416, 416)

model = Darknet(cfg_path, img_size)

torch.save(model, 'gezi-detector.pt')
